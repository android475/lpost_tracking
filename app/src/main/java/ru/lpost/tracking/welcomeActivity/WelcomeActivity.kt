package ru.lpost.tracking.welcomeActivity

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.animation.AnimationUtils
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import ru.lpost.tracking.R
import ru.lpost.tracking.mainActivity.MainActivity

class WelcomeActivity: AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome)
        findViewById<ImageView>(R.id.welcomeLogo).startAnimation(AnimationUtils.loadAnimation(this, R.anim.fadein))
        Handler(Looper.getMainLooper()).postDelayed({
            startActivity(Intent(this, MainActivity::class.java))
        }, 2900)
    }
}