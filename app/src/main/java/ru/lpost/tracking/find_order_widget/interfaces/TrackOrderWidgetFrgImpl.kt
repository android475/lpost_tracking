package ru.lpost.tracking.find_order_widget.interfaces

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment

abstract class TrackOrderWidgetFrgImpl: Fragment(){

    private var trackOrderWidgetPresenter: IntTrackOrderWidgetPresenter? = null

    abstract fun getIdLayoutFragment(): Int

    fun setPresenter(pp: IntTrackOrderWidgetPresenter) {
        trackOrderWidgetPresenter = pp
    }

    fun getPresenter(): IntTrackOrderWidgetPresenter {
        return trackOrderWidgetPresenter!!
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(getIdLayoutFragment(),container, false)
    }
}