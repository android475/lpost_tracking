package ru.lpost.tracking.find_order_widget.view

import ru.lpost.tracking.R
import ru.lpost.tracking.find_order_widget.interfaces.FindOrderFrgImpl

class FindOrderByNumberFrg(): FindOrderFrgImpl() {

    override fun getIdErrorLabel(): Int {
        return R.id.errMess_NumberFrg
    }

    override fun getIdFindEditText(): Int {
        return R.id.edOrderNum
    }

    override fun getIdBtnFind(): Int {
        return R.id.btnFind
    }

    override fun getIdLayoutFragment(): Int {
        return R.layout.fragment_find_order_by_number
    }

    override fun getOrderNumber(): String {
        return getFindData()
    }

    override fun getPhoneNumber(): String? {
        return null
    }
}