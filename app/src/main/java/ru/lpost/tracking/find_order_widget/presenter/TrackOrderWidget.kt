package ru.lpost.tracking.find_order_widget.presenter

import androidx.fragment.app.Fragment
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import ru.lpost.tracking.R
import ru.lpost.tracking.find_order_widget.interfaces.*
import ru.lpost.tracking.find_order_widget.model.OrderRepo
import ru.lpost.tracking.find_order_widget.view.FindOrderByPhoneFrg
import ru.lpost.tracking.find_order_widget.view.FindOrderByNumberFrg
import ru.lpost.tracking.find_order_widget.view.FindOrderResultFrg
import ru.lpost.tracking.find_order_widget.view.WaitLoadingFrg
import ru.lpost.tracking.mainActivity.MainActivityImpl

class TrackOrderWidget(_parentActivity: MainActivityImpl):
    IntTrackOrderWidgetPresenter {
    private val parentActivity = _parentActivity
    private var frgCurrent: Fragment? = null

    private fun showWaitFrg(){
        showTrackOrderWidgetFrg(WaitLoadingFrg())
    }

    override fun getContext(): MainActivityImpl {
        return parentActivity
    }

    private fun showFindOrderByPhoneFrg(_orderNumber: String){
        val fop = FindOrderByPhoneFrg()
        fop.setPresenter(this)
        fop.setOrderNumber(_orderNumber)
        showTrackOrderWidgetFrg(fop)
    }

    override fun showFindOrderByNumberFrg(){
        val fon = FindOrderByNumberFrg()
        fon.setPresenter(this)
        showTrackOrderWidgetFrg(fon)
    }

    private fun showFindOrderResult(_json: JSONObject){
        var json = _json
        val forFrg = FindOrderResultFrg()
        forFrg.setPresenter(this)

        // Максимально тупая структура из бд. при курьерке все лежит в data. при самовывозе внутри data лежит trackingData, в котором всё что нам нужно
        json = if (json.has("trackingData")) json.getJSONObject("trackingData") else json

        if(_json.has("pickupData")){
            val jsonPickUp = _json.getJSONObject("pickupData")

            val workHoursJson = JSONArray(jsonPickUp.getString("work_hours"))
            var workHours = ""
            var workDays = ""
            (0 until workHoursJson.length()).forEach{
                workDays += "${workHoursJson.getJSONObject(it).getString("full")}\n"
                workHours += "${workHoursJson.getJSONObject(it).getString("from")} - ${workHoursJson.getJSONObject(it).getString("to")}\n"
            }
            forFrg.workDays = workDays
            forFrg.workHours = workHours
            forFrg.isCash = jsonPickUp.getInt("is_cash") == 1
            forFrg.isCard = jsonPickUp.getInt("is_card") == 1
            forFrg.address = jsonPickUp.getString("address")
            forFrg.addressDescription = jsonPickUp.getString("location_description")
        }

        forFrg.orderNum = json.getString("CustomerNumber")
        forFrg.orderState = json.getString("TXT_Status_RU")
        forFrg.deliveryDate = if (json.isNull("DeliveryDatePlan")) null else json.getString("DeliveryDatePlan")
        forFrg.takeFrom = if (json.isNull("TimeFrom")) null else json.getString("TimeFrom")
        forFrg.takeTo = if (json.isNull("TimeTo")) null else json.getString("TimeTo")
        forFrg.isCourier = json.getString("isCourier") == "1"
        showTrackOrderWidgetFrg(forFrg)
    }

    override fun showTrackOrderWidgetFrg(frg: TrackOrderWidgetFrgImpl){
        parentActivity.supportFragmentManager.beginTransaction().replace(parentActivity.getPlacementIdContainer(), frg).commitNow()
        frgCurrent = frg
    }

    override fun findClick(vv: FindOrderFrgImpl, _orderNum: String, _phoneNum: String?) {
        showWaitFrg()
        parentActivity.hideBtnNewFind()
        val orderRepo = OrderRepo()
        Thread {
            orderRepo.loadDataFromRepo(_orderNum, _phoneNum)
            Thread.sleep(250)
            if (orderRepo.responseCode != 200){
                parentActivity.runOnUiThread{
                    showTrackOrderWidgetFrg(vv)
                    vv.showErrorLabel("Ошибка! ${orderRepo.responseString}")
                    parentActivity.unHideBtnNewFind()
                }
            }
            else if (orderRepo.responseData.isEmpty()){
                parentActivity.runOnUiThread{
                    showTrackOrderWidgetFrg(vv)
                    vv.showErrorLabel("Ошибка! Пустой ответ от сервера")
                    parentActivity.unHideBtnNewFind()
                }
            }
            else{
                try {
                    val responseJSON = JSONObject(orderRepo.responseData)
                    val errorCode = responseJSON.getString("error").toIntOrNull()

                    if (errorCode == 2){
                        parentActivity.runOnUiThread{
                            showFindOrderByPhoneFrg(_orderNum)
                            parentActivity.unHideBtnNewFind()
                        }
                    }
                    else if (errorCode == 3){
                        parentActivity.runOnUiThread{
                            showTrackOrderWidgetFrg(vv)
                            vv.showErrorLabel("Заказ не найден. Проверьте номер телефона и попробуйте снова,\n" +
                                    "или свяжитесь с поддержкой ${parentActivity.resources.getString(R.string.CC_phone)}")
                            parentActivity.unHideBtnNewFind()
                        }
                    }
                    else if (errorCode == 4){
                        parentActivity.runOnUiThread{
                            showTrackOrderWidgetFrg(vv)
                            vv.showErrorLabel("Ошибка запроса! Попробуйте снова")
                            parentActivity.unHideBtnNewFind()
                        }
                    }
                    // Смотрим на длинну, ибо при пустой data прилетает объект массив [], а при заполненном объект не массив {}
                    else if (responseJSON.getString("data").length <= 2){
                        parentActivity.runOnUiThread{
                            showTrackOrderWidgetFrg(vv)
                            vv.showErrorLabel("Ошибка! Пустой ответ от сервера")
                            parentActivity.unHideBtnNewFind()
                        }
                    }
                    else{
                        parentActivity.runOnUiThread{
                            showFindOrderResult(responseJSON.getJSONObject("data"))
                            parentActivity.unHideBtnNewFind()
                        }
                    }
                }
                catch (j: JSONException) {
                    parentActivity.runOnUiThread{
                        showTrackOrderWidgetFrg(vv)
                        vv.showErrorLabel("Ошибка! $j")
                        parentActivity.unHideBtnNewFind()
                    }
                }
            }
        }.start()
    }

    override fun getFrgCurrent(): Fragment? {
        return frgCurrent
    }
}