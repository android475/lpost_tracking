package ru.lpost.tracking.find_order_widget.view

import ru.lpost.tracking.R
import ru.lpost.tracking.find_order_widget.interfaces.TrackOrderWidgetFrgImpl

class WaitLoadingFrg: TrackOrderWidgetFrgImpl() {
    override fun getIdLayoutFragment(): Int {
        return R.layout.fragment_waiting
    }
}