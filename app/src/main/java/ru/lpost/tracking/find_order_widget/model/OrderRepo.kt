package ru.lpost.tracking.find_order_widget.model


import org.json.JSONObject
import org.jsoup.Connection
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.jsoup.select.Elements
import ru.lpost.tracking.ConstsClass
import ru.lpost.tracking.find_order_widget.interfaces.IntOrderRepo
import java.io.IOException


class OrderRepo: IntOrderRepo{
    var responseCode = 0
    var responseString = ""
    var responseData = ""
    private var requestToken = ""
    private var requestCookies: MutableMap<String, String> = mutableMapOf()

    override fun loadDataFromRepo(_orderNum: String, _phoneNum: String?) {
        clearAllData()
        requestGetHeaders()
        requestGetData(_orderNum, _phoneNum)
    }

    private fun requestGetHeaders() {
        requestToken = ""
        requestCookies = mutableMapOf()
        try {
            val resHeaders: Connection.Response = Jsoup.connect(ConstsClass.FIND_ORDER_URL).ignoreHttpErrors(true).execute()
            val docHeaders: Document = resHeaders.parse()

            requestCookies = resHeaders.cookies()
            val metaElements: Elements = docHeaders.select("meta")
            for (e in metaElements){
                if (e.attr("name") == "csrf-token"){
                    requestToken = e.attr("content")
                }
            }
        } catch (e: IOException) {
            requestToken = ""
            requestCookies = mutableMapOf()
        }
    }

    private fun requestGetData(_orderNum: String, _phoneNum: String?) {
        val jsonParams = JSONObject()
        jsonParams.put("trackNumber", _orderNum)
        jsonParams.put("phoneNumber", if (_phoneNum.isNullOrEmpty()) "" else _phoneNum)
        val requestBody = JSONObject().put("OrderTracking", jsonParams).toString()
        try {
            val resDataConn: Connection = Jsoup.connect(ConstsClass.FIND_ORDER_URL).ignoreHttpErrors(true).ignoreContentType(true)
                .cookies(requestCookies)
                .method(Connection.Method.POST)
                .header("x-csrf-token", requestToken)
                .header("content-type", "application/json")
                .requestBody(requestBody)

            val resData: Connection.Response = resDataConn.execute()
            val docData: Document = resData.parse()
            responseCode = resData.statusCode()
            responseString = resData.statusMessage()
            if(responseCode == 200){
                responseData = docData.body().text()
            }
        } catch (e: IOException) {
            responseCode = -1
            responseString = "ERROR IO $e"
        }
    }

    private fun clearAllData(){
        responseCode = 0
        responseString = ""
        responseData = ""
        requestToken = ""
        requestCookies = mutableMapOf()
    }
}