package ru.lpost.tracking.find_order_widget.view

import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.style.UnderlineSpan
import android.view.View
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import ru.lpost.tracking.R
import ru.lpost.tracking.find_order_widget.interfaces.TrackOrderWidgetFrgImpl
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class FindOrderResultFrg: TrackOrderWidgetFrgImpl() {

    var orderNum: String = ""
    var isCourier: Boolean = false
    var takeFrom: String? = ""
    var takeTo: String? = ""
    var deliveryDate: String? = ""
    var orderState: String = ""
    var isCash: Boolean = false
    var isCard: Boolean = false
    var address: String = ""
    var addressDescription: String = ""
    var workHours: String = ""
    var workDays: String = ""

    override fun getIdLayoutFragment(): Int {
        return R.layout.fragment_find_order_result
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.setBackgroundResource(R.drawable.round_corner)
        val vAddress = view.findViewById<TextView>(R.id.tvAddress)

        orderNum = savedInstanceState?.getString("orderNum") ?: orderNum
        orderState = savedInstanceState?.getString("orderState", orderState) ?: orderState
        addressDescription = savedInstanceState?.getString("addressDescription", addressDescription) ?: addressDescription
        address = savedInstanceState?.getString("address", address) ?: address
        workHours = savedInstanceState?.getString("workHours", workHours) ?: workHours
        workDays = savedInstanceState?.getString("workDays", workDays) ?: workDays
        takeFrom = savedInstanceState?.getString("takeFrom", takeFrom) ?: takeFrom
        takeTo = savedInstanceState?.getString("takeTo", takeTo) ?: takeTo
        deliveryDate = savedInstanceState?.getString("deliveryDate", deliveryDate) ?: deliveryDate
        isCourier = savedInstanceState?.getBoolean("isCourier", isCourier) ?: isCourier
        isCash = savedInstanceState?.getBoolean("isCash", isCash) ?: isCash
        isCard = savedInstanceState?.getBoolean("isCard", isCard) ?: isCard

        if (deliveryDate.isNullOrEmpty() or takeFrom.isNullOrEmpty() or takeTo.isNullOrEmpty()){
            view.findViewById<TextView>(R.id.tvDeliveryDate).text = "Дата будет известна позднее"
        }
        else{
             val formattedDate: String = DateFormat.getDateInstance(DateFormat.FULL).format(SimpleDateFormat("yyyy-MM-dd", Locale.US).parse(deliveryDate!!)!!)
             val splitDate = formattedDate.split(",").toTypedArray()
             val deliveryDateFormated = splitDate[1] + " (${splitDate[0]})"
             view.findViewById<TextView>(R.id.tvDeliveryDate).text = String.format(resources.getString(R.string.deliveryDate), deliveryDateFormated, takeFrom, takeTo)
        }
        view.findViewById<TextView>(R.id.orderResultHeader).text = String.format(resources.getString(R.string.order_result_header), orderNum, if (isCourier) "Курьерская доставка" else "Самовывоз")
        view.findViewById<TextView>(R.id.tvOrderState).text = String.format(resources.getString(R.string.orderState), orderState)

        // Если самовывоз - показываем инфо по нему
        view.findViewById<ConstraintLayout>(R.id.clPS_Address).visibility = View.GONE
        if(!isCourier){
            val formatedAddress = String.format(resources.getString(R.string.addressDescription), address, addressDescription)
            val underlinedFormatedAddress = SpannableString(formatedAddress)
            underlinedFormatedAddress.setSpan(UnderlineSpan(), 0, SpannableString(formatedAddress).length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            vAddress.text = underlinedFormatedAddress
            vAddress.setOnClickListener { getPresenter().getContext().addressIntent(address)}
            view.findViewById<TextView>(R.id.tvPaymentMode).text = String.format(resources.getString(R.string.payment_mode), if(isCash) "Оплата наличными" else "" + if(isCard) " или картой" else "")
            view.findViewById<TextView>(R.id.tvWorkDays).text = String.format(resources.getString(R.string.workDays), workDays)
            view.findViewById<TextView>(R.id.tvWorkHours).text = String.format(resources.getString(R.string.workHours), workHours)
            view.findViewById<ConstraintLayout>(R.id.clPS_Address).visibility = View.VISIBLE
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putString("orderNum", orderNum)
        outState.putString("orderState", orderState)
        outState.putString("addressDescription", addressDescription)
        outState.putString("address", address)
        outState.putString("workHours", workHours)
        outState.putString("workDays", workDays)
        outState.putString("takeFrom", takeFrom)
        outState.putString("takeTo", takeTo)
        outState.putString("deliveryDate", deliveryDate)
        outState.putBoolean("isCourier", isCourier)
        outState.putBoolean("isCash", isCash)
        outState.putBoolean("isCard", isCard)
        super.onSaveInstanceState(outState)
    }
}