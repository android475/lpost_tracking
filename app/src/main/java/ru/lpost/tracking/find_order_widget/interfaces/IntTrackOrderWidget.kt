package ru.lpost.tracking.find_order_widget.interfaces

import androidx.fragment.app.Fragment
import ru.lpost.tracking.mainActivity.MainActivityImpl

// Логика виджета
interface IntTrackOrderWidgetPresenter{
    fun findClick(vv: FindOrderFrgImpl, _orderNum: String, _phoneNum: String?)
    fun getFrgCurrent(): Fragment?
    fun showFindOrderByNumberFrg()
    fun showTrackOrderWidgetFrg(frg: TrackOrderWidgetFrgImpl)
    fun getContext(): MainActivityImpl
}

// Репо с данными из БД
interface IntOrderRepo{
    fun loadDataFromRepo(_orderNum: String, _phoneNum: String?)
}