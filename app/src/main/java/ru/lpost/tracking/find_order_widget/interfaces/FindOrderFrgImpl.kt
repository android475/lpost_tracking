package ru.lpost.tracking.find_order_widget.interfaces

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import ru.lpost.tracking.R

abstract class FindOrderFrgImpl: TrackOrderWidgetFrgImpl() {

    abstract fun getIdErrorLabel(): Int
    abstract fun getIdFindEditText(): Int
    abstract fun getIdBtnFind(): Int
    abstract fun getOrderNumber(): String
    abstract fun getPhoneNumber(): String?

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val vErrLabel = view.findViewById<TextView>(getIdErrorLabel())
        view.findViewById<EditText>(getIdFindEditText()).setText(savedInstanceState?.getString("findData"))
        vErrLabel.text = savedInstanceState?.getString("errViewText")
        vErrLabel.visibility = savedInstanceState?.getInt("errViewVisibility") ?: View.GONE

        view.setBackgroundResource(R.drawable.round_corner)
        view.findViewById<Button>(getIdBtnFind()).setOnClickListener{
            hideErrorLabel()
            val dataToFind = getFindData()
            when (validFindData(dataToFind)) {
                1 -> {
                    showErrorLabel("Введите номер отправления")
                }
                else -> {
                    getPresenter().findClick(this, getOrderNumber(), getPhoneNumber())
                }
            }
        }
    }

    private fun hideErrorLabel() {
        view?.findViewById<TextView>(getIdErrorLabel())?.visibility = View.GONE
    }

    fun showErrorLabel(_errMsg: String) {
        val tvErr = view?.findViewById<TextView>(getIdErrorLabel())
        tvErr?.text = _errMsg
        tvErr?.visibility = View.VISIBLE
    }

    private fun validFindData(_findData: String?): Int {
        return when {
            _findData.isNullOrEmpty() -> 1
            else -> 0
        }
    }

    fun getFindData(): String {
        return view?.findViewById<EditText>(getIdFindEditText())?.text.toString()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putString("findData", getFindData())
        outState.putString("errViewText", requireView().findViewById<TextView>(getIdErrorLabel()).text.toString())
        outState.putInt("errViewVisibility", requireView().findViewById<TextView>(getIdErrorLabel()).visibility)
        super.onSaveInstanceState(outState)
    }
}