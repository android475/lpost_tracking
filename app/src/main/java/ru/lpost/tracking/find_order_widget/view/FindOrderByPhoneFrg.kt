package ru.lpost.tracking.find_order_widget.view

import android.os.Bundle
import android.telephony.PhoneNumberFormattingTextWatcher
import android.view.View
import android.widget.EditText
import android.widget.TextView
import ru.lpost.tracking.R
import ru.lpost.tracking.find_order_widget.interfaces.FindOrderFrgImpl

class FindOrderByPhoneFrg(): FindOrderFrgImpl() {

    private var orderNumber = ""

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setOrderNumber(savedInstanceState?.getString("orderNumber") ?: orderNumber)
        view.findViewById<TextView>(R.id.tvOrderNumHeader).text = String.format(resources.getString(R.string.defaultNumOrder), orderNumber)
        view.findViewById<EditText>(getIdFindEditText()).addTextChangedListener(
            PhoneNumberFormattingTextWatcher()
        )
    }

    override fun getIdLayoutFragment(): Int {
        return R.layout.fragment_find_order_by_phone
    }

    override fun getIdErrorLabel(): Int {
        return R.id.errMess_PhoneFrg
    }

    override fun getIdFindEditText(): Int {
        return R.id.edPhoneNumber
    }

    override fun getIdBtnFind(): Int {
        return R.id.BtnFindClarifyPhone
    }

    override fun getOrderNumber(): String {
        return orderNumber
    }

    override fun getPhoneNumber(): String {
        return reformatPhoneNumber(getFindData())
    }

    fun setOrderNumber(_orderNumber: String){
        orderNumber = _orderNumber
    }

    private fun reformatPhoneNumber(_phoneNumber: String): String{
        var phoneNumber: String = _phoneNumber.replace(" ","", true).replace("-","", true)
        phoneNumber = when {
            (phoneNumber.length < 11 ) -> "+7$phoneNumber"
            (phoneNumber.length == 11 ) -> "+7${phoneNumber.substring(1)}"
            else -> phoneNumber
        }
        return phoneNumber
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putString("orderNumber", getOrderNumber())
        super.onSaveInstanceState(outState)
    }
}