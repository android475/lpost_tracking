package ru.lpost.tracking.mainActivity

import android.content.Intent
import android.net.Uri
import androidx.core.content.ContextCompat

class MainActPresenter<V: MainActivityImpl>: IntMainActPresenter<V> {

    private var view: V? = null

    override fun attachView(vv: V) {
        view = vv
    }

    override fun detachView() {
        view = null
    }

    override fun phoneIntent(context: MainActivityImpl, phoneNum: String){
        val intPhoneCall = Intent(Intent.ACTION_DIAL)
        intPhoneCall.data = Uri.parse("tel:$phoneNum")
        ContextCompat.startActivity(context, intPhoneCall, null)
    }

    override fun addressIntent(context: MainActivityImpl, address: String) {
        val geoUriString = "geo:0,0?q=$address&z=8"
        val geoIntent = Intent(Intent.ACTION_VIEW, Uri.parse(geoUriString))
        ContextCompat.startActivity(context, geoIntent, null)
    }
}