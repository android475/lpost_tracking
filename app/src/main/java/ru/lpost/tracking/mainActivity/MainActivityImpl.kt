package ru.lpost.tracking.mainActivity

import androidx.appcompat.app.AppCompatActivity

abstract class MainActivityImpl: AppCompatActivity() {
    abstract fun hideBtnNewFind()
    abstract fun unHideBtnNewFind()
    abstract fun getPlacementIdContainer(): Int
    abstract fun addressIntent(address: String)
}

interface IntMainActPresenter<V: MainActivityImpl>{
    fun attachView(vv: V)
    fun detachView()
    fun phoneIntent(context: MainActivityImpl, phoneNum: String)
    fun addressIntent(context: MainActivityImpl, address: String)
}