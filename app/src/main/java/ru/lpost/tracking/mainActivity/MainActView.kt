package ru.lpost.tracking.mainActivity


import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import ru.lpost.tracking.ConstsClass.Companion.TRACK_ORDER_WIDGET_LAST_FRAGMENT_TAG
import ru.lpost.tracking.R
import ru.lpost.tracking.find_order_widget.interfaces.TrackOrderWidgetFrgImpl
import ru.lpost.tracking.find_order_widget.presenter.TrackOrderWidget


class MainActivity: MainActivityImpl() {

    private val presenter = MainActPresenter<MainActivity>()
    private val trackOrdWdg = TrackOrderWidget(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val tvHeadCCPhone = findViewById<TextView>(R.id.tvHeadCCPhone)
        val btnNewFind = findViewById<Button>(R.id.btnNewFind)
        btnNewFind.setOnClickListener {
            trackOrdWdg.showFindOrderByNumberFrg()
            hideBtnNewFind()
        }
        btnNewFind.visibility = savedInstanceState?.getInt("btnNewFindVisibility") ?: View.GONE
        tvHeadCCPhone.setOnClickListener { presenter.phoneIntent(this, tvHeadCCPhone.text.toString()) }

        // Если в бандл пришла вьюха что была открыта ранее - переоткрываем её
        if (savedInstanceState?.let {supportFragmentManager.getFragment(it, TRACK_ORDER_WIDGET_LAST_FRAGMENT_TAG)} != null){
            val vv = supportFragmentManager.getFragment(savedInstanceState, TRACK_ORDER_WIDGET_LAST_FRAGMENT_TAG)!!
            (vv as TrackOrderWidgetFrgImpl).setPresenter(trackOrdWdg)
            trackOrdWdg.showTrackOrderWidgetFrg(vv)
        }
        else {
            trackOrdWdg.showFindOrderByNumberFrg()
        }
        presenter.attachView(this)
    }

    override fun onDestroy() {
        presenter.detachView()
        super.onDestroy()
    }

    override fun hideBtnNewFind(){
        findViewById<Button>(R.id.btnNewFind).visibility = View.GONE
    }

    override fun unHideBtnNewFind(){
        findViewById<Button>(R.id.btnNewFind).visibility = View.VISIBLE
    }

    override fun getPlacementIdContainer(): Int {
        return R.id.MainLayoutCenter
    }

    override fun addressIntent(address: String) {
        presenter.addressIntent(this, address)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        trackOrdWdg.getFrgCurrent()?.let {supportFragmentManager.putFragment(outState, TRACK_ORDER_WIDGET_LAST_FRAGMENT_TAG, it)}
        outState.putInt("btnNewFindVisibility", findViewById<Button>(R.id.btnNewFind).visibility)
        super.onSaveInstanceState(outState)
    }
}