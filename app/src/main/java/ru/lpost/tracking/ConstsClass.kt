package ru.lpost.tracking

abstract class ConstsClass {
    companion object {
        const val FIND_ORDER_URL = "https://l-post.ru/tracking/check"
        const val LOG_TAG = "LOG_TAG"
        const val TRACK_ORDER_WIDGET_LAST_FRAGMENT_TAG = "TRACK_ORDER_WIDGET_LAST_FRAGMENT_TAG"
    }
}